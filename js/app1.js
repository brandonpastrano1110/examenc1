//manejando promesas 
llamandoFetch = () => {
    
    const tipoN = document.querySelector('input[name="tipo"]:checked');
    if (!tipoN) {
        alert("No se seleccione ningun tipo de bebida");
        return;
    }
    const tipo = document.querySelector('input[name="tipo"]:checked').id;
    let url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?";
    if (tipo == 'alc') {
        url += 'a=Alcoholic';
    }
    else if (tipo == 'nalc') {
        url += 'a=Non_Alcoholic';
    }
    fetch(url)
        .then(respuesta => respuesta.json())
        .then(data => mostrar(data))
        .catch((reject) => {

            console.log("Surgió el siguiente error: " + reject);
        });
}
function mostrar(data) {
    const mostrarSection = document.getElementById("mostrar");
    mostrarSection.innerHTML = '';
    let total = 0;

    data.drinks.forEach(cocktel => {
        const nombre = cocktel.strDrink;
        const imagen = cocktel.strDrinkThumb;

        const divMostrar = document.createElement('div');
        divMostrar.classList.add('cocktel');

        const nombreCocktel = document.createElement('p');
        nombreCocktel.textContent = nombre;
        const imgCocktel = document.createElement('img');
        imgCocktel.src = imagen;
        imgCocktel.alt = nombre;
        divMostrar.appendChild(imgCocktel);
        divMostrar.appendChild(nombreCocktel);
        mostrarSection.appendChild(divMostrar);
        total++;
    });
    const totalP = document.getElementById("total");
    totalP.textContent = `Total de cócteles: ${total}`;
}
document.getElementById("btnListar").addEventListener('click', function () {
    llamandoFetch();
});
document.getElementById("btnLimpiar").addEventListener('click', function () {
    const mostrarSection = document.getElementById("mostrar");
    mostrarSection.innerHTML = '';
});